.PHONY: all main clean

all: thesis

run: thesis

thesis: | ${BUILD_DIR}
	pdflatex  poster.tex

clean:
	rm -rf poster.pdf

